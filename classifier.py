"""
===================================================
     Introduction to Machine Learning (67577)
             IML HACKATHON, June 2017

            **  Headline Classifier  **

Auther(s):

===================================================
"""
from sklearn.cross_validation import train_test_split
from sklearn.feature_extraction.text import CountVectorizer
from nltk.corpus import stopwords
from nltk import PorterStemmer
from sklearn.feature_extraction.text import TfidfTransformer
from sklearn.linear_model import SGDClassifier
from sklearn.discriminant_analysis import LinearDiscriminantAnalysis
from sklearn.neural_network import MLPClassifier
from sklearn.neighbors import KNeighborsClassifier
from sklearn.svm import SVC
from sklearn.tree import DecisionTreeClassifier
from sklearn.naive_bayes import GaussianNB
from sklearn.ensemble import voting_classifier
from sklearn.svm import LinearSVC
from sklearn.pipeline import Pipeline
from sklearn.model_selection import GridSearchCV
from sklearn.linear_model import LogisticRegression
from sklearn.ensemble import RandomForestClassifier, VotingClassifier
from sklearn.model_selection import cross_val_score
import numpy as np
import math
from sklearn.cross_validation import train_test_split
import os

os.system("python3 -m nltk.downloader stopwords")


haaretz_file = "haaretz.csv"
israelhayom_file = "israelhayom.csv"
STOP_WORDS = stopwords.words('english')

HAARETZ = 0
ISRAEL_HAYOM = 1
TRAIN_FRAC = 0.8

class Classifier(object):

    def __init__(self):
        self.vectorizer = None

        f1 = open(haaretz_file)
        f2 = open(israelhayom_file)
        lines1 = f1.readlines()
        lines2 = f2.readlines()

        lines_with_class = [(line, HAARETZ) for line in lines1] + [(line, ISRAEL_HAYOM) for line in lines2]

        self.dictionary_maker(lines_with_class)

        self.train, self.test = self.split_data(lines_with_class, TRAIN_FRAC)


        self.preprocessed_train = self.preprocess([line[0] for line in self.train])

        self.preprocessed_test = self.preprocess([line[0] for line in self.test])


        clf2 = MLPClassifier()
        clf1 = LinearSVC()
        clf3 = SGDClassifier()


        self.model = VotingClassifier(estimators=[('ml', clf1), ('lg', clf2), ('sgd', clf3)], voting='hard').fit(self.preprocessed_train.toarray(), [line_vec[1] for line_vec in self.train])

    def train_score(self):
        # print(self.preprocessed_train.shape)
        # print(len(self.train))
        return self.model.score(self.preprocessed_train, [t[1] for t in self.train])

    def test_score(self):
        # print(self.preprocessed_test.shape)
        # print(len(self.test))
        return self.model.score(self.preprocessed_test, [t[1] for t in self.test])

    def score_on_samples(self, samples_with_class):
        return self.model.score(
            [s[0] for s in samples_with_class],
            [s[1] for s in samples_with_class]
        )

    def preprocess(self, headlines):

        # print('Number of lines: ' + str(len(headlines)))
        line_vecs = [
            (headline, CountVectorizer().build_tokenizer()(headline))
            for headline in headlines
            ]
        filtered_line_vecs = [(line_vec[0], self.clean_vector(line_vec[1])) for line_vec in line_vecs]
        # for i in range(25):
            # print(filtered_line_vecs[i])

        t = [' '.join(line_vec[1]) for line_vec in filtered_line_vecs]
        if self.vectorizer is None:
            self.vectorizer = CountVectorizer(min_df=1, ngram_range=(1,2))
            self.vectorizer.fit(t)

        counts = self.vectorizer.transform(t)

        return TfidfTransformer(use_idf=False).fit_transform(
            counts
        )

    def split_data(self, headlines_with_class, train_frac):
        np.random.shuffle(headlines_with_class)
        num_train = math.floor(len(headlines_with_class) * train_frac)

        train = headlines_with_class[:num_train]
        test = headlines_with_class[num_train:]

        return train, test

    def dictionary_maker(self,lines_with_class):
        dictionary = {}
        for i in range(len(lines_with_class)):
            lines_with_class[i] = (lines_with_class[i][0].split(" "), lines_with_class[i][1])
            for j in range(len(lines_with_class[i][0])):
                lines_with_class[i][0][j] = PorterStemmer().stem(lines_with_class[i][0][j].lower().strip())
                if lines_with_class[i][0][j] in dictionary:
                    if lines_with_class[i][1] == HAARETZ:
                        dictionary[lines_with_class[i][0][j]] += 1
                    else:
                        dictionary[lines_with_class[i][0][j]] -= 1

                else:
                    if lines_with_class[i][1] == HAARETZ:
                        dictionary[lines_with_class[i][0][j]] = 1
                    else:
                        dictionary[lines_with_class[i][0][j]] = -1
        for i in range(len(lines_with_class)):
            for word in lines_with_class[i][0]:
                if 4 > dictionary[word] > -4:
                    lines_with_class[i][0].remove(word)
            lines_with_class[i] = (" ".join(lines_with_class[i][0]),lines_with_class[i][1])
        return lines_with_class


    def remove_all(self, list_of_things_to_remove, list):
        return [i for i in list if i not in list_of_things_to_remove]

    def clean_vector(self, line_vec):
        return self.add_info( self.remove_numbers(self.remove_stop_words(
            self.stems(self.to_lowercase(line_vec)))))

    def add_doubles(self,line_vec):
        length = len(line_vec)
        for i in range(length -1):
            line_vec.append(" ".join((line_vec[i],line_vec[i+1])))
        return line_vec

    def add_info(self,line_vec):
        sum = 0
        for word in line_vec:
            sum+= len(word)
        avg= sum/(len(line_vec)+1)
        line_vec.append(str(avg))
        line_vec.append(str(len(line_vec)))
        return line_vec

    def remove_stop_words(self, line_vec):
        return self.remove_all(STOP_WORDS, line_vec)

    def remove_numbers(self, line_vec):
        return [i for i in line_vec if not i.isdigit()]

    def stems(self, line_vec):
        return [PorterStemmer().stem(i) for i in line_vec]

    def to_lowercase(self, line_vec):
        return [i.lower() for i in line_vec]

    def classify(self,X):
        """
        Recieves a list of m unclassified headlines, and predicts for each one which newspaper published it.
        :param X: A list of length m containing the headlines' texts (strings)
        :return: y_hat - a binary vector of length m
        """
        p = self.preprocess([line for line in X])
        return self.model.predict(p)


if __name__ == "__main__":

    c = Classifier()
    print("Train accuracy (score): " + str(c.train_score()))
    print("Test accuracy (score): " + str(c.test_score()))

    print("Prediction for title:")
    rs = c.classify([
        "Firebrand Anti-leftist Group, Which Slams Foreign Funding of Israeli NGOs, Received Over $1M From U.S. Donors",
        "Hundreds of former Israeli generals warn U.S. Taylor Force Act could create security risks for Israel",
        "Empty book on Palestinian history becomes instant best-seller on Amazon",
        "Kushner returns to U.S. after discussing Israeli-Palestinian peace with Netanyahu, Abbas Palestinians dismayed after Kushner-Abbas meet in Ramallah",
        "Israel Electric Chief on Cutting Power to Gaza Hospitals: 'It's Our Job'",
        "IDF: Hezbollah Is Setting Up a Weapons Industry in Lebanon With Iran's Help",
        "In Victory for BDS, U.K. High Court Lets Councils Opt Out of Investing in Israel",
        "Assaf Harel Asks Israelis What They Would Do if They Were Palestinians",
        "'Israeli Spring': Ehud Barak Says Time Is Ripe for Broad Front to Topple Netanyahu Government",
        "Iranians Hold Annual anti-Israel Rallies to Mark al-Quds Day",


        # Israel Hayom:




        "	IAF chief: Israel to go 'all-out' in future war with Hezbollah",
     	"Public security minister unveils plan to foil attacks in Jerusalem",
        "As ties with Hamas thaw, Egypt sends emergency fuel to Gaza ",
        "Turkish Temple Mount takeover bid prompts MKs to take action ",
        "Israel poised to build 7,000 homes beyond the Green Line ",
        "Haredi man suspected of vandalizing Jerusalem synagogue",
        "Haredim told intransigence on Western Wall could topple government",
        "Germany delays decision on Israeli drones, approves warships",
        "ISIS destroys historic mosque where it declared 'caliphate'",
        "'We passed anti-BDS laws thanks to Israelis in the US'"

    ])
    print(rs)
    print(['Haartez' if r == HAARETZ else "Israel Hayom" for r in rs])


